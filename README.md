# eratosthenesblog

This is my humble tech blog, to keep track of adventures and details I might find useful or interesting in the future.

I took my last real-name blog offline when it became awkward to discuss in employment contexts, so this time we won't be using my real name.

Once everything is working, the markdown I write in this repo should end up nicely formatted and statically hosted at eratosthenes.blog :)

Enjoy!